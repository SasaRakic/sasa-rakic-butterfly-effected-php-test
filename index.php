<?php

require_once(dirname(__FILE__) . "/loader.php");

$htmlTitle = "Home page";

$FORM = array();
if (isset($_POST) && isset($_GET)) {
  $FORM = array_merge($_POST, $_GET);
} else {
  $FORM = array_merge($HTTP_POST_VARS, $HTTP_GET_VARS);
}

$formBoostrapWidth = array(
  "col-xs" => 12, // 1..12, Extra small devices Phones (<768px) 
  "col-sm" => 12, // 1..12, Small devices Tablets (>=768px)
  "col-md" => 12, // 1..12, Medium devices Desktops (>=992px)
  "col-lg" => 12 // 1..12, Large devices Desktops (>=1200px)   
);
$formBoostrapGroup = "col-xs-12 col-sm-6 col-md-6 col-lg-6";

if (isset($_SERVER) &&
  isset($_SERVER['REQUEST_METHOD']) &&
  strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') === 0) {

  $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : "";

  if (strcasecmp($contentType, "application/json") === 0) {
    $jsonContent = trim(file_get_contents("php://input"));
    $jsonArray = json_decode($jsonContent);
    if (json_last_error() === JSON_ERROR_NONE) {
      foreach ($jsonArray as $tag => $value) {
        $FORM[$tag] = $value;
      }
    }
  }
}

if (isset($FORM["model"]) &&
  isset($FORM["action"])) {
  if ($FORM["model"] === "User") {
    $ctrlUser = new UserController($FORM);
  };
}

require_once(dirname(__FILE__) . "/" . DIRECTORY_VIEWS . "/header.part.phtml");
require_once(dirname(__FILE__) . "/" . DIRECTORY_VIEWS . "/User/user-add.form.phtml");
require_once(dirname(__FILE__) . "/" . DIRECTORY_VIEWS . "/User/users-view.table.phtml");
require_once(dirname(__FILE__) . "/" . DIRECTORY_VIEWS . "/footer.part.phtml");

?>