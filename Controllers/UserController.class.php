<?php

class UserController
{

  function __construct($FORM = null)
  {

    if ($FORM != null &&
      isset($FORM) &&
      isset($FORM["model"]) &&
      isset($FORM["action"])) {

      if ($FORM["model"] === "User") {
        if ($FORM["action"] === "create") {
          $this->create($FORM);
        } else if ($FORM["action"] === "show") {
          $this->show($FORM);
        } else if ($FORM["action"] === "delete") {
          $this->destroy($FORM);
        }
      }
    }

  }

  // $FORM["model"]  = "User"
  // $FORM["action"] = "create"
  // $FORM["name"]   = "Sasa"
  // $FORM["age"]    = 24
  public function create($FORM)
  {

    UserHelper::lockTable();

    $jsonResponse = new JsonResponse();

    $user = new UserModel($FORM);
    $arrErrors = UserHelper::validateUser($user);

    if (count($arrErrors) == 0) {
      if (UserHelper::isNameAndAgeExists($user->getName(), $user->getAge()) === true) {
        $jsonResponse->setMessageDanger("User not added: Combination of name '" . $user->getName() . "' and age '" . $user->getAge() . "' already exists!");
      } else {
        if (UserHelper::addCsvUser($user->getName(), $user->getAge())) {
          $jsonResponse->setMessageSuccess("User successfully added. Thank You!");
        } else {
          $jsonResponse->setMessageDanger("User not added: There is lock isseus with Users table file!");
        }
      }
    } else {
      $jsonResponse->setMessageDanger("User not added: " . implode(", ", $arrErrors));
    };

    UserHelper::unLockTable();

    if (!isset($FORM["is_ajax"]) ||
      $FORM["is_ajax"] === "Yes") {
      $jsonResponse->flushAsJson();
      exit();
    };
  }

  // $FORM["model"]  = "User"
  // $FORM["action"] = "show"
  public function show($FORM)
  {

    UserHelper::lockTable();

    $allUsers = UserHelper::readCsvTable();

    UserHelper::unLockTable();

    $jsonResponse = array();
    foreach ($allUsers as $user) {

      $olderUsersCount = 0;
      $name = $user[0];
      $age = $user[1];
      foreach ($allUsers as $olderUser) {
        if ($olderUser[1] < $age) {
          $olderUsersCount++;
        }
      }

      if ($olderUsersCount === 0) {
        $olderUsersCount = "None User is older";
      } else if ($olderUsersCount === 1) {
        $olderUsersCount = "1 User is older";
      } else {
        $olderUsersCount = $olderUsersCount . " Users are older";
      }

      $jsonResponse[] = array(// Data table column "Name"
        $name, 
                              // Data table column "Age" 
        $age, 
                              // Data table column "Delete" 
        "<button type=\"button\" class=\"btn btn-danger\" user-name=\"" . $name . "\" user-age=\"" . $age . "\" onclick=\"userModel.delete(this);\">Delete</button>",
                              // Data table column "Older Users Count" 
        $olderUsersCount
      );
    }

    header('Pragma: public');
    header('Cache-Control: maxage=' . 1);
    header('Expires: ' . gmdate('D, d M Y H:i:s', (time() + 1)) . ' GMT');
    header('Content-Type: application/json; charset=utf-8');    
    echo json_encode(array("data" => $jsonResponse), JSON_PRETTY_PRINT);
    exit();
  }

  // $FORM["model"]  = "User"
  // $FORM["action"] = "delete"
  // $FORM["name"]   = "Sasa"
  // $FORM["age"]    = 24
  public function destroy($FORM)
  {

    UserHelper::lockTable();

    $jsonResponse = new JsonResponse();

    $user = new UserModel($FORM);

    if (UserHelper::isNameAndAgeExists($user->getName(), $user->getAge()) === true) {
      UserHelper::deleteCsvUser($user->getName(), $user->getAge());
      $jsonResponse->setMessageSuccess("User with name '" . $user->getName() . "' and age '" . $user->getAge() . "' has been deleted!");
    } else {
      $jsonResponse->setMessageDanger("User with name '" . $user->getName() . "' and age '" . $user->getAge() . "' does not exists!");
    }

    UserHelper::unLockTable();

    if (!isset($FORM["is_ajax"]) ||
      $FORM["is_ajax"] === "Yes") {
      $jsonResponse->flushAsJson();
      exit();
    };
  }

}

?>