<?php

define("DIRECTORY_CLASSES", "Classes");
define("DIRECTORY_CONTROLLERS", "Controllers");
define("DIRECTORY_DATABASE", "Database");
define("DIRECTORY_HELPERS", "Helpers");
define("DIRECTORY_MODELS", "Models");
define("DIRECTORY_VIEWS", "Views");

define("LOCK_TABLE_SLEEP_TIME_IN_MICROSECONDS", 100);
define("LOCK_TABLE_MAX_LOCK_WAIT_TIME_IN_SECONDS", 4);

?>