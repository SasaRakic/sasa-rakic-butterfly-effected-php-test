<?php

require_once(dirname(__FILE__) . "/configuration.php");

$classesRootDirectoriesNames = array(
  DIRECTORY_CLASSES,
  DIRECTORY_CONTROLLERS,
  DIRECTORY_HELPERS,
  DIRECTORY_MODELS
);

foreach ($classesRootDirectoriesNames as $directoryName) {

  $directoryPath = dirname(__FILE__) . "/" . $directoryName . "/";

  $directoryHandle = opendir($directoryPath) or die("Unable to open directory path '" . $directoryPath . "'");
  while ($fileName = readdir($directoryHandle)) {

    if (file_exists($directoryPath . $fileName) &&
      $fileName != "." &&
      $fileName != ".." &&
      is_file($directoryPath . $fileName) &&
      strlen($fileName) > strlen(".class.php") &&
      substr($fileName, (0 - strlen(".class.php"))) === ".class.php") {

      require_once($directoryPath . $fileName);

    }
  };
  closedir($directoryHandle);

};

?>