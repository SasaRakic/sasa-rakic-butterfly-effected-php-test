<?php

class UserModel
{
  private $name = "";
  private $age = 18;

  function __construct($FORM = null)
  {
    if ($FORM != null &&
      isset($FORM)) {
      if (isset($FORM["name"])) {
        $this->setName($FORM["name"]);
      }
      if (isset($FORM["age"])) {
        $this->setAge($FORM["age"]);
      }
    }
  }

  public function __get($property)
  {
    if ($property == "name") {
      return $this->name;
    } else if ($property == "age") {
      return $this->age;
    }
    return "";
  }

  public function __set($property, $value)
  {
    if ($property == "name") {
      $this->name = $value;
    } else if ($property == "age") {
      $this->age = $value;
    }
  }

  public function getName()
  {
    return $this->name;
  }
  public function setName($value)
  {
    $this->name = $value;
  }

  public function getAge()
  {
    return $this->age;
  }
  public function setAge($value)
  {
    $this->age = $value;
  }

  public function toString()
  {
    $result = "Name: " . $this->getName() . ",\r\n" .
      "Age:  " . $this->getAge() . "\r\n";
    return $result;
  }

}


?>