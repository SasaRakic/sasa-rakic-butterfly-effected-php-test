<?php

class UserHelper
{

  function __construct()
  {
  }

  public static function getTablePath()
  {
    return dirname(__FILE__) . "/../" . DIRECTORY_DATABASE . "/users.table.csv";
  }

  public static function getLockedTablePath()
  {
    return UserHelper::getTablePath() . ".locked";
  }

  public static function lockTable()
  {

    if (file_exists(UserHelper::getLockedTablePath())) {

      list($microseconds, $seconds) = explode(" ", microtime());

      while (file_exists(UserHelper::getLockedTablePath())) {

        usleep(LOCK_TABLE_SLEEP_TIME_IN_MICROSECONDS);

        list($newMicroseconds, $newSeconds) = explode(" ", microtime());
        if (($newSeconds - $seconds) > LOCK_TABLE_MAX_LOCK_WAIT_TIME_IN_SECONDS) {
          unlink(UserHelper::getLockedTablePath());
          break;
        }

      };
    }

    $fp = fopen(UserHelper::getLockedTablePath(), 'w');
    fclose($fp);
  }

  public static function unLockTable()
  {
    unlink(UserHelper::getLockedTablePath());
  }

  public static function readCsvTable($skipHeader = true)
  {

    $resultLines = array();

    $fh = fopen(UserHelper::getTablePath(), 'r');
    $row = 0;
    while (!feof($fh)) {
      $line = fgetcsv($fh, 10000);
      if (isset($line[0]) &&
        !is_null($line[0])) {
        if ($row == 0 && $skipHeader == true) {
          // Skip header
        } else {
          $resultLines[] = $line;
        }
      };
      $row++;
    }
    fclose($fh);

    return $resultLines;
  }

  public static function writeCsvTable($csvLines, $writeHeader = true)
  {
    $fh = fopen(UserHelper::getTablePath(), 'w');
    if ($writeHeader === true) {
      fputcsv($fh, array('Name', 'Age'));
    }
    foreach ($csvLines as $csvLine) {
      fputcsv($fh, $csvLine);
    }
    fclose($fh);
  }

  public static function isNameAndAgeExists($name, $age)
  {
    $csvLines = UserHelper::readCsvTable();
    foreach ($csvLines as $csvLine) {
      $csvName = $csvLine[0];
      $csvAge = $csvLine[1];
      if ($csvName == $name &&
        $csvAge == $age) {
        return true;
      };
    }
    return false;
  }

  public static function addCsvUser($name, $age)
  {
    $fh = fopen(UserHelper::getTablePath(), "a");
    if ($fh) {
      fputcsv($fh, array($name, $age));
      fclose($fh);
      return true;
    };
    return false;
  }

  public static function deleteCsvUser($name, $age)
  {

    $csvLines = UserHelper::readCsvTable();

    $newCsvLines = array();
    $isUserDeleted = false;
    foreach ($csvLines as $csvLine) {
      $csvName = $csvLine[0];
      $csvAge = $csvLine[1];
      if ($csvName == $name &&
        $csvAge == $age) {
        // skip/delee this csv line
        $isUserDeleted = true;
      } else {
        $newCsvLines[] = array($csvName, $csvAge);
      }
    }

    UserHelper::writeCsvTable($newCsvLines, true);

    return $isUserDeleted;
  }

  public static function validateUser($user)
  {
    $result = array();

    if (preg_match("/\s/", $user->getName())) {
      $result[] = "Name can not have spaces!";
    };
    if (strlen($user->getName()) < 2) {
      $result[] = "Name must have more than " . 1 . " letter!";
    };
    if (strlen($user->getName()) > 30) {
      $result[] = "Name can not have more than " . 30 . " letters!";
    };

    if (is_numeric($user->getAge()) === false) {
      $result[] = "Age must be number!";
    };
    if (strlen("" . $user->getAge()) > 3) {
      $result[] = "Age can not have more than 3 digits!";
    };
    if (intval($user->getAge()) < 0) {
      $result[] = "Age can not be less than " . 0 . " years!";
    };
    if (intval($user->getAge()) > 120) {
      $result[] = "Age can not be more than " . 120 . " years!";
    };

    return $result;
  }

}

?>