<?php

require_once(dirname(__FILE__) . "/loader.php");

$FORM = array();
if (isset($_POST) && isset($_GET)) {
  $FORM = array_merge($_POST, $_GET);
} else {
  $FORM = array_merge($HTTP_POST_VARS, $HTTP_GET_VARS);
}

if (isset($_SERVER) &&
  isset($_SERVER['REQUEST_METHOD']) &&
  strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') === 0) {

  $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : "";

  if (strcasecmp($contentType, "application/json") === 0) {
    $jsonContent = trim(file_get_contents("php://input"));
    $jsonArray = json_decode($jsonContent);
    if (json_last_error() === JSON_ERROR_NONE) {
      foreach ($jsonArray as $tag => $value) {
        $FORM[$tag] = $value;
      }
    }
  }
}

if (isset($FORM["model"]) &&
  isset($FORM["action"])) {
  if ($FORM["model"] === "User") {
    $ctrlUser = new UserController($FORM);
  };
}

$jsonResponse = new JsonResponse();
$jsonResponse->setMessageDanger("Wrong API call: Use POST-JSON parameters -> { 'model' : 'User', 'action' : 'create,show,delete', 'name' : 'abcd...', 'age' : 0-120 }");
$jsonResponse->flushAsJson();
exit();

?>