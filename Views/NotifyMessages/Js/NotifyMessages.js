function NotifyMessages() {

  this.initialize = function () {

    jQuery("#message_success_x_close").click(function () {
      jQuery("#message_success_area").hide(500);
    });

    jQuery("#message_info_x_close").click(function () {
      jQuery("#message_info_area").hide(500);
    });

    jQuery("#message_warning_x_close").click(function () {
      jQuery("#message_warning_area").hide(500);
    });

    jQuery("#message_danger_x_close").click(function () {
      jQuery("#message_danger_area").hide(500);
    });

  };

  this.showAllMessage = function (json) {
    notifyMessages.showMessageSuccess(json.message_success.title, json.message_success.body);
    notifyMessages.showMessageInfo(json.message_info.title, json.message_info.body);
    notifyMessages.showMessageWarning(json.message_warning.title, json.message_warning.body);
    notifyMessages.showMessageDanger(json.message_danger.title, json.message_danger.body);
  };

  this.hideAllMessages = function () {
    jQuery("#message_success_area").hide(500);
    jQuery("#message_info_area").hide(500);
    jQuery("#message_warning_area").hide(500);
    jQuery("#message_danger_area").hide(500);
  };

  this.showMessageSuccess = function (messageTitleSuccess, messageBodySuccess) {
    if (messageBodySuccess != null &&
      messageBodySuccess != "") {
      if (messageTitleSuccess == "") {
        jQuery("#message_title_success").html("Success!");
      } else {
        jQuery("#message_title_success").html(messageTitleSuccess);
      };
      jQuery("#message_body_success").html(messageBodySuccess);
      jQuery("#message_success_area").show(500);
    } else {
      jQuery("#message_success_area").hide(500);
    };
  };

  this.showMessageInfo = function (messageTitleInfo, messageBodyInfo) {
    if (messageBodyInfo != null &&
      messageBodyInfo != "") {
      if (messageTitleInfo == "") {
        jQuery("#message_title_info").html("Success!");
      } else {
        jQuery("#message_title_info").html(messageTitleInfo);
      };
      jQuery("#message_body_info").html(messageBodyInfo);
      jQuery("#message_info_area").show(500);
    } else {
      jQuery("#message_info_area").hide(500);
    };
  };

  this.showMessageWarning = function (messageTitleWarning, messageBodyWarning) {
    if (messageBodyWarning != null &&
      messageBodyWarning != "") {
      if (messageTitleWarning == "") {
        jQuery("#message_title_warning").html("Warning!");
      } else {
        jQuery("#message_title_warning").html(messageTitleWarning);
      };
      jQuery("#message_body_warning").html(messageBodyWarning);
      jQuery("#message_warning_area").show(500);
    } else {
      jQuery("#message_warning_area").hide(500);
    };
  };

  this.showMessageDanger = function (messageTitleDanger, messageBodyDanger) {
    if (messageBodyDanger != null &&
      messageBodyDanger != "") {
      if (messageTitleDanger == "") {
        jQuery("#message_title_danger").html("Danger!");
      } else {
        jQuery("#message_title_danger").html(messageTitleDanger);
      };
      jQuery("#message_body_danger").html(messageBodyDanger);
      jQuery("#message_danger_area").show(500);
    } else {
      jQuery("#message_danger_area").hide(500);
    };
  };

};

var notifyMessages = new NotifyMessages();

jQuery(document).ready(function () {
  notifyMessages.initialize();
})