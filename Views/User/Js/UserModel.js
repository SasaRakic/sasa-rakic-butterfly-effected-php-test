function UserModel() {

  this.initialize = function () {

    jQuery("#userName").keyup(function () {
      notifyMessages.showMessageWarning("User Name validate", userModel.validateName(false));
    });
    jQuery("#userName").change(function () {
      notifyMessages.showMessageWarning("User Name validate", userModel.validateName(false));
    });

    jQuery("#userAge").keyup(function () {
      notifyMessages.showMessageWarning("User Age validate", userModel.validateAge());
    });
    jQuery("#userAge").change(function () {
      notifyMessages.showMessageWarning("User Age validate", userModel.validateAge());
    });

    jQuery("#userDataTable").DataTable({
      "paging": true,
      "ordering": false,
      "info": true,
      "stateSave": true,
      "pagingType": "full_numbers",
      "lengthMenu": [
        [ /*10, */ 20 /*, 25, 50, -1*/ ],
        [ /*10, */ 20, /*25, 50, "All"*/ ]
      ],
      "pageLength": 20,
      "ajax": "user-api.php?model=User&action=show",
      "deferRender": true,
      "fnInitComplete": function (oSettings, json) {
        userModel.changeAgeLessThan18Color();
      }
    });
  };

  this.changeAgeLessThan18Color = function () {
    jQuery.each(jQuery("#userDataTable tbody tr td:nth-child(2)"),
      function (index, value) {
        if (parseInt(jQuery(this).html()) < 18) {
          jQuery(this).parent("tr").css("background-color", "pink");
        }
      }
    );
  }

  this.reloadUsers = function () {
    jQuery("#userDataTable").DataTable().ajax.reload(function (json) {
        userModel.changeAgeLessThan18Color();
      },
      false);
  }

  this.delete = function (deleteButtonJs) {
    if (confirm("Do you realy want to delete user with Name '" +
        jQuery(deleteButtonJs).attr("user-name") +
        "' and age of '" +
        jQuery(deleteButtonJs).attr("user-age") + "' ?")) {

      jQuery.ajax({
        url: jQuery("#userCreateForm").attr("action"),
        data: {
          "is_ajax": "Yes",
          "model": "User",
          "action": "delete",
          "name": jQuery(deleteButtonJs).attr("user-name"),
          "age": jQuery(deleteButtonJs).attr("user-age"),
        },
        // Whether this is a POST or GET request
        type: "GET",
        contentType: "application/json",
        success: function (json) {
          notifyMessages.showAllMessage(json);
          userModel.reloadUsers();
          if (json.redirect_url != "") {
            document.location.href = json.redirect_url;
          };
        },
        error: function (xhr, status, errorThrown) {
          alert("Sorry, there was a error/problem!\r\n" + xhr + "\r\n" + errorThrown + "\r\n" + status + "\r\n");
          console.log("Error: " + errorThrown);
          console.log("Status: " + status);
          console.dir(xhr.responseText);
        },
        // Code to run if the request fails; the raw request and
        // status codes are passed to the function
        fail: function (xhr, status, errorThrown) {
          jQuery("#messageAlert").html("Sorry, there was a problem!");
          alert("Sorry, there was a problem!");
          console.log("Error: " + errorThrown);
          console.log("Status: " + status);
          console.dir(xhr);
        }
      });
    };
  };

  this.validateName = function (validateLengthAlso) {
    var nameField = jQuery("#userName");
    var nameFieldValue = nameField.val();
    if (/\s/.test(nameFieldValue)) {
      return "Name can not have spaces!";
    };
    if (validateLengthAlso == true && nameFieldValue.length < 2) {
      return "Name must have more than " + 1 + " letter!";
    };
    if (nameFieldValue.length > parseInt(nameField.attr("maxlength"))) {
      return "Name can not have more than " + nameField.attr("maxlength") + " letters!";
    };
    return "";
  };

  this.validateAge = function () {
    var ageField = jQuery("#userAge");
    var ageFieldValue = ageField.val();
    if (isNaN(parseInt(ageFieldValue))) {
      return "Age must be number!";
    };
    if (ageFieldValue.length > 3) {
      return "Age can not have more than 3 digits!";
    };
    if (parseInt(ageFieldValue) < parseInt(ageField.attr("min"))) {
      return "Age can not be less than " + ageField.attr("min") + " years!";
    };
    if (parseInt(ageFieldValue) > parseInt(ageField.attr("max"))) {
      return "Age can not be more than " + ageField.attr("max") + " years!";
    };
    return "";
  };

  this.create = function () {

    if (userModel.validateName(true) != "") {
      notifyMessages.showMessageWarning("User Name validate", userModel.validateName(true));
      return false;
    };
    if (userModel.validateAge() != "") {
      notifyMessages.showMessageWarning("User Name validate", userModel.validateAge());
      return false;
    };

    jQuery("#userCreateForm").ajaxSubmit({ // The URL for the request
      url: jQuery("#userCreateForm").attr("action"),
      // The data to send (will be converted to a query string)
      data: {
        "is_ajax": "Yes"
      },
      // Whether this is a POST or GET request
      type: "POST",
      // The type of data we expect back
      dataType: "json",
      cache: false,
      // Code to run if the request succeeds (is done);
      // The response is passed to the function
      success: function (json) {
        notifyMessages.showAllMessage(json);
        userModel.reloadUsers();
        if (json.redirect_url != "") {
          document.location.href = json.redirect_url;
        };
      },
      error: function (xhr, status, errorThrown) {
        alert("Sorry, there was a error/problem!\r\n" + xhr + "\r\n" + errorThrown + "\r\n" + status + "\r\n");
        console.log("Error: " + errorThrown);
        console.log("Status: " + status);
        console.dir(xhr.responseText);
      },
      // Code to run if the request fails; the raw request and
      // status codes are passed to the function
      fail: function (xhr, status, errorThrown) {
        jQuery("#messageAlert").html("Sorry, there was a problem!");
        alert("Sorry, there was a problem!");
        console.log("Error: " + errorThrown);
        console.log("Status: " + status);
        console.dir(xhr);
      }
    });
    return false;
  };

};

var userModel = new UserModel();

jQuery(document).ready(function () {
  userModel.initialize();
})