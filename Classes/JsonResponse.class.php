<?php

class JsonResponse
{
  private $jsonResponse = array(
    "success" => false,
    "description" => "",
    "redirect_url" => "",
    "message_success" => array(
      "title" => "Success!",
      "body" => "",
    ),
    "message_info" => array(
      "title" => "Info!",
      "body" => "",
    ),
    "message_warning" => array(
      "title" => "Warning!",
      "body" => "",
    ),
    "message_danger" => array(
      "title" => "Danger!",
      "body" => "",
    )
  );

  function __construct()
  {
  }

  public function toJson($jsonPrettyPrint = true)
  {
    if ($jsonPrettyPrint === true) {
      return json_encode($this->jsonResponse, JSON_PRETTY_PRINT);
    } else {
      return json_encode($this->jsonResponse);
    };
  }

  public function flushAsJson()
  {
    header("Content-Type: application/json");
    echo $this->toJson();
  }

  public function clear()
  {
    $this->jsonResponse = array(
      "success" => false,
      "redirect_url" => "",
      "message_success" => array(
        "title" => "Success!",
        "body" => "",
      ),
      "message_info" => array(
        "title" => "Info!",
        "body" => "",
      ),
      "message_warning" => array(
        "title" => "Warning!",
        "body" => "",
      ),
      "message_danger" => array(
        "title" => "Danger!",
        "body" => "",
      )
    );
  }

  public function setSuccess($success)
  {
    $this->jsonResponse["success"] = $success;
  }
  public function getSuccess()
  {
    return $this->jsonResponse["success"];
  }

  public function setRedirectUrl($url)
  {
    $this->jsonResponse["redirect_url"] = $url;
  }
  public function getRedirectUrl()
  {
    return $this->jsonResponse["redirect_url"];
  }

  public function setMessageSuccess($body = "", $title = null)
  {
    if ($title !== null) {
      $this->jsonResponse["message_success"]["title"] = $title;
    }
    if ($body !== null) {
      $this->jsonResponse["message_success"]["body"] = $body;
      if ($body !== "") {
        $this->jsonResponse["success"] = true;
        $this->jsonResponse["description"] = $body;
      }
    }
  }
  public function getMessageSuccess()
  {
    return $this->jsonResponse["message_success"];
  }

  public function setMessageInfo($body = "", $title = null)
  {
    if ($title !== null) {
      $this->jsonResponse["message_info"]["title"] = $title;
    }
    if ($body !== null) {
      $this->jsonResponse["message_info"]["body"] = $body;
    }
  }
  public function getMessageInfo()
  {
    return $this->jsonResponse["message_info"];
  }

  public function setMessageWarning($body = "", $title = null)
  {
    if ($title !== null) {
      $this->jsonResponse["message_warning"]["title"] = $title;
    }
    if ($body !== null) {
      $this->jsonResponse["message_warning"]["body"] = $body;
    }
  }
  public function getMessageWarning()
  {
    return $this->jsonResponse["message_warning"];
  }

  public function setMessageDanger($body = "", $title = null)
  {
    if ($title !== null) {
      $this->jsonResponse["message_danger"]["title"] = $title;
    }
    if ($body !== null) {
      $this->jsonResponse["message_danger"]["body"] = $body;
      if ($body !== "") {
        $this->jsonResponse["success"] = false;
        $this->jsonResponse["description"] = $body;
      }
    }
  }
  public function getMessageDanger()
  {
    return $this->jsonResponse["message_danger"];
  }

}

?>